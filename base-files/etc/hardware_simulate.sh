#!/bin/sh

echo "Hardware simulating script for peripheral manager.."

gpio_module=$(lsmod | grep gpio-mockup)
i2c_module=$(lsmod | grep i2c_stub)

if [ -z "$gpio_module" ] && [ -z "$i2c_module"]; then
  	echo "Probe not happened properly"
  	exit
else

  echo "Setting active_low for gpio reset"

  echo 1 > /sys/class/gpio/gpio18/active_low && \
 
  echo "Setting active_low for status green led"
  echo 1 > /sys/class/gpio/gpio8/active_low && \

  echo "Setting active_low for WPS_green led"
  echo 1 > /sys/class/gpio/gpio12/active_low && \


x=1
 while [ $x -le 3 ]
do
  echo "Start the button simulating.."
  
  echo 0 > /sys/class/gpio/gpio18/active_low
  sleep 2
  echo 1 > /sys/class/gpio/gpio18/active_low
  sleep 2
  
  yes | i2cset 0 0x2b 0x00 0x10 b
  yes | i2cset 0 0x2b 0x01 0x08 b
 yes |  i2cset 0 0x2b 0x02 0x80 b

  sleep 1

  yes | i2cset 0 0x2b 0x00 0x20 b
  yes | i2cset 0 0x2b 0x01 0x00 b
  yes | i2cset 0 0x2b 0x02 0x00 b


sleep 1

yes | i2cset 0 0x2b 0x00 0x00 b
yes | i2cset 0 0x2b 0x02 0x00 b
yes | i2cset 0 0x2b 0x01 0x00 b

sleep 1

x=$(( $x + 1 ))

done

fi     
