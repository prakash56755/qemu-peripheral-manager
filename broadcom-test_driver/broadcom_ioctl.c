#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>


#include "broadcom_ioctl.h"

#define FIRST_MINOR 0
#define MINOR_CNT 1

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;

static int my_open(struct inode *i, struct file *f)
{
	return 0;
}
static int my_close(struct inode *i, struct file *f)
{
	return 0;
}

static long my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)

{

	BOARD_IOCTL_PARMS ctrlParms;

	switch (cmd)
	{
	case BOARD_IOCTL_GET_GPIO:

		if (copy_from_user((void *)&ctrlParms, (void *)arg, sizeof(ctrlParms)) == 0)
		{

			if (ctrlParms.strLen >= 0 && ctrlParms.strLen <= 20)
				ctrlParms.result = 0;
			else if (ctrlParms.strLen >= 21 && ctrlParms.strLen <= 60)
				ctrlParms.result = 0;
			else if (ctrlParms.strLen >= 61 && ctrlParms.strLen <= 90)
				ctrlParms.result = 1;
			else if (ctrlParms.strLen >= 91 && ctrlParms.strLen <= 150)
				ctrlParms.result = 0;
			else
				ctrlParms.result = 0;

			if (copy_to_user((BOARD_IOCTL_PARMS *)arg, &ctrlParms, sizeof(BOARD_IOCTL_PARMS)))
			{
				return -EACCES;
			}
		}
		else
			return -EACCES;

		break;

	case BOARD_IOCTL_SET_GPIO:
		if (copy_from_user((void *)&ctrlParms, (void *)arg, sizeof(ctrlParms)) == 0)
		{

			ctrlParms.result = 0;

			if (copy_to_user((BOARD_IOCTL_PARMS *)arg, &ctrlParms, sizeof(BOARD_IOCTL_PARMS)))
			{
				return -EACCES;
			}
		}
		else
			return -EACCES;
		break;
		
	default:
		return -EINVAL;
	}

	return 0;
}

static struct file_operations brcm_ops =
	{
		.owner = THIS_MODULE,
		.open = my_open,
		.release = my_close,
		.unlocked_ioctl = my_ioctl

};

static int __init brcm_ioctl_init(void)
{
	int ret;
	struct device *dev_ret;

	if ((ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "brcmboard_ioctl")) < 0)
	{
		return ret;
	}

	cdev_init(&c_dev, &brcm_ops);

	if ((ret = cdev_add(&c_dev, dev, MINOR_CNT)) < 0)
	{
		return ret;
	}

	if (IS_ERR(cl = class_create(THIS_MODULE, "brcm_test")))
	{
		cdev_del(&c_dev);
		unregister_chrdev_region(dev, MINOR_CNT);
		return PTR_ERR(cl);
	}
	if (IS_ERR(dev_ret = device_create(cl, NULL, dev, NULL, "brcmboard")))
	{
		class_destroy(cl);
		cdev_del(&c_dev);
		unregister_chrdev_region(dev, MINOR_CNT);
		return PTR_ERR(dev_ret);
	}

	return 0;
}

static void __exit brcm_ioctl_exit(void)
{
	device_destroy(cl, dev);
	class_destroy(cl);
	cdev_del(&c_dev);
	unregister_chrdev_region(dev, MINOR_CNT);
}

module_init(brcm_ioctl_init);
module_exit(brcm_ioctl_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pitchaiah  <pitchaiah@iopsys.eu>");
MODULE_DESCRIPTION("brcm test ioctl Char Driver");
