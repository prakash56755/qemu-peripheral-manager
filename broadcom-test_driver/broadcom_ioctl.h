/*****************************************************************************/
/*          board ioctl calls for flash, led and some other utilities        */
/*****************************************************************************/
/* Defines. for board driver */

#include <linux/ioctl.h>

#define BOARD_IOCTL_MAGIC       'B'


#define BOARD_IOCTL_SET_GPIO                    _IOWR(BOARD_IOCTL_MAGIC, 26, BOARD_IOCTL_PARMS)
#define BOARD_IOCTL_GET_GPIO                    _IOWR(BOARD_IOCTL_MAGIC, 126,BOARD_IOCTL_PARMS )


// for the action in BOARD_IOCTL_PARMS for flash operation
typedef enum 
{
    PERSISTENT,
    NVRAM,
    BCM_IMAGE_CFE,
    BCM_IMAGE_FS,
    BCM_IMAGE_KERNEL,
    BCM_IMAGE_WHOLE,
    SCRATCH_PAD,
    FLASH_SIZE,
    SET_CS_PARAM,
    BACKUP_PSI,
    SYSLOG,
    ENVRAM
} BOARD_IOCTL_ACTION;
    
typedef struct boardIoctParms
{
    union {
	    char *string;
	    char *value;
	};
    union {
      char *buf;
      char *param;
    };
    union {
        int strLen;
        int value_length;
    };
    int offset;
    BOARD_IOCTL_ACTION  action;        /* flash read/write: nvram, persistent, bcm image */
    int result;
} BOARD_IOCTL_PARMS;


